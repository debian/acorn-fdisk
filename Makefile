STRIP		=strip
#CFLAGS		=-DUNIX -DVERSION=\"$(VERSION)\" -O2 -Wall -fomit-frame-pointer -Ilib -pipe
CFLAGS		=-DUNIX -DVERSION=\"$(VERSION)\" -O2 -Wall -g -Ilib -pipe
LDFLAGS		=-N
LIBS		=lib/part.a lib/scheme.a lib/blkio.a lib/util.a
TAR		=tar
VERSION		=3.0.6

FDISKOBJS	=fdisk.o #partitions.o utils.o

all:		fdisk

clean:;		$(RM) fdisk *.o
		@$(MAKE) $@ -C lib/part
		@$(MAKE) $@ -C lib/scheme
		@$(MAKE) $@ -C lib/blkio
		@$(MAKE) $@ -C lib/util

tar:		clean
		(cd ..; $(TAR) zcf arm-fdisk-$(VERSION).tar.gz arm-fdisk-$(VERSION))

fdisk:		$(FDISKOBJS) $(LIBS)
		$(CC) $(LDFLAGS) -o $@ $(FDISKOBJS) $(LIBS)
		$(STRIP) --discard-locals $@

.PHONY: lib/part.a lib/scheme.a lib/blkio.a lib/util.a

lib/part.a:;	@$(MAKE) -C lib/part
lib/scheme.a:;	@$(MAKE) -C lib/scheme
lib/blkio.a:;	@$(MAKE) -C lib/blkio
lib/util.a:;	@$(MAKE) -C lib/util

