/*
 * blkio/blkio.h
 *
 * Copyright (C) 1998 Russell King
 *
 * The block I/O interface allows a OS-independent method to access devices
 *
 * Public structures for block I/O based routines
 */
#ifndef BLKIO_BLKIO_H
#define BLKIO_BLKIO_H

#include "util/types.h"

typedef u_int blk_t;

typedef struct {
  u_int cylinders;
  u_int heads;
  u_int sectors;
  u_int sector_size;			/* read-only! */
  u_int log2secsize;			/* read-only! */
} geometry_t;

typedef struct {
#ifdef UNIX
  /* Unix specific I/O data */
  int fd;				/* File descriptor */
#endif
#ifdef RISCOS
  /* RiscOS specific I/O data */
  u_int		swi_describedisc;	/* SWI number for xxx_DescribeDisc	*/
  u_int		swi_discop;		/* SWI number for xxx_DiscOp		*/
  u_int		swi_sectorop;		/* SWI number for xxx_SectorOp		*/
  u_int		swi_controllertype;	/* SWI number for xxx_ControllerType	*/
  u_int		swi_ideuserop;		/* SWI number for xxx_IDEUserOp		*/
  union disc_record *disc_record;	/* Disc Record to pass with DiscOp/SectorOp */
  u_int		drive;
#endif
  geometry_t	geometry;
  int		blocksize;		/* Size of a block */
} blkio_t;

/* Function: blkio_t *blkio_close (blkio_t *blkio)
 * Purpose : close a device previously opened with blkio_open
 * Params  : blkio  - structure returned by blkio_open
 * Returns : NULL
 */
extern blkio_t *blkio_close (blkio_t *blkio);

/* Function: u_int blkio_getgeometry (blkio_t *blkio, geometry_t *geo)
 * Purpose : Return the geometry for the device opened with blkio_open
 * Params  : blkio - structure returned by blkio_open
 *         : geo   - pointer to geometry structure
 */
extern u_int blkio_getgeometry (blkio_t *blkio, geometry_t *geo);

/* Function: blkio_t *blkio_open (const char *dev_name)
 * Purpose : Open a device, specified in dev_name and return a structure
 * Params  : dev_name - device name (eg, ADFS::4, /dev/hda etc)
 * Returns : pointer to above structure (to be used by all other calls)
 */
extern blkio_t *blkio_open (const char *dev_name);

/* Function: u_int blkio_setblocksize (blkio_t *blkio, u_int blocksize)
 * Purpose : Set the block size for all subsequent block IO routines
 * Params  : blkio     - structure returned by blkio_open
 *         : blocksize - new block size to use
 * Returns : block size that will be used
 */
extern u_int blkio_setblocksize (blkio_t *blkio, u_int blocksize);

/* Function: u_int blkio_setgeometry (blkio_t *blkio, geometry_t *geo)
 * Purpose : Set the geometry for the device opened with blkio_open
 * Params  : blkio - structure returned by blkio_open
 *         : geo   - pointer to geometry structure
 */
extern u_int blkio_setgeometry (blkio_t *blkio, const geometry_t *geo);

/* Function: u_int blkio_read (blkio_t *blkio, void *data, blk_t block, u_int nr_blocks)
 * Purpose : Read `nr_blocks' blocks starting at `block' from the device previously opened
 *           with blkio_open into memory area `data'
 * Params  : blkio     - structure returned by blkio_open
 *         : data      - memory area to read into
 *         : block     - block number to read
 *         : nr_blocks - number of blocks to read
 * Returns : number of blocks actually read
 */
extern u_int blkio_read (blkio_t *blkio, void *data, blk_t block, u_int nr_blocks);

/* Function: u_int blkio_write (blkio_t *blkio, const void *data, blk_t block, u_int nr_blocks)
 * Purpose : Write `nr_blocks' blocks starting at `block' from the device previously opened
 *           with blkio_open into memory area `data'
 * Params  : blkio     - structure returned by blkio_open
 *         : data      - memory area to write data from
 *         : block     - block number to write
 *         : nr_blocks - number of blocks to write
 * Returns : number of blocks actually written
 */
extern u_int blkio_write (blkio_t *blkio, const void *data, blk_t block, u_int nr_blocks);

#endif
