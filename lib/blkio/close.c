/*
 * blkio/close.c
 *
 * Copyright (C) 1998 Russell King
 *
 * The block I/O interface allows an OS-independent method to access devices
 *
 * Close a device on UNIX architecture
 */
#include <stdlib.h>
#include <unistd.h>

#include "util/debug.h"
#include "blkio.h"

/* Function: blkio_t *blkio_close (blkio_t *blkio)
 * Purpose : close a device previously opened with blkio_open
 * Params  : blkio  - structure returned by blkio_open
 * Returns : NULL
 */
blkio_t *
blkio_close(blkio_t *blkio)
{
  dbg_printf("blkio_close()");
  if (blkio) {
#if defined(RISCOS)
    free(blkio->disc_record);
#elif defined(UNIX)
    close(blkio->fd);
#endif
    free(blkio);
  }
  dbg_printf("ret=NULL");
  return NULL;
}
