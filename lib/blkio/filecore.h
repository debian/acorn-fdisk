/*
 * Structure of a filecore disc record
 */
#ifndef BLKIO_FILECORE_H
#define BLKIO_FILECORE_H

#include "util/types.h"

#define FILECORE_BOOT_SECTOR	6
#define FILECORE_SECTOR_SIZE	512

typedef union disc_record {
  struct {
    u8	log2secsize;		/* sector size */
    u8	secspertrack;		/* sectors per track */
    u8	heads;			/* number of heads */
    u8	density;		/* density of disk */
    u8	idlen;
    u8	log2bpmb;		/* bits per map bit */
    u8	skew;
    u8	bootoption;		/* boot option */
    u8	lowsector;		/* lowest numbered sector on track */
    u8	nzones;			/* number of map zones */
    u16	zone_spare;
    u32	root;			/* address of root directory */
    u32	disc_size;		/* disc size low word */
    u16	disc_id;		/* disc cycle id */
    u8	disc_name[10];		/* disc name */
    u32	disctype;		/* disc type */
    u32	disc_size_high;		/* disc size high word */
    unsigned int	share_size : 4,		/* granularity of directory sharing */
			unused     : 4,
			big_flag   : 1,		/* 1: use LBA mode */
			unused2    : 23;
  } d;
  u8 packing[64];
} disc_record_t;

#endif
