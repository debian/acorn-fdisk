/*
 * blkio/getgeometry.c
 *
 * Copyright (C) 1998 Russell King
 *
 * The block I/O interface allows an OS-independent method to access devices
 *
 * Close a device on RiscOS architecture
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "blkio.h"

#include "filecore.h"

/* Function: u_int blkio_getgeometry (blkio_t *blkio, geometry_t *geo)
 * Purpose : Return the geometry for the device opened with blkio_open
 * Params  : blkio - structure returned by blkio_open
 *         : geo   - pointer to geometry structure
 */
u_int blkio_getgeometry (blkio_t *blkio, geometry_t *geo)
{
  assert (blkio != NULL);
  assert (geo != NULL);

  dbg_printf ("blkio_getgeometry()");
  *geo = blkio->geometry;
  dbg_printf ("ret=[sector_size=%d, sectors=%d, heads=%d, cylinders=%d]",
	      geo->sector_size, geo->sectors, geo->heads, geo->cylinders);

  return !0;
}
