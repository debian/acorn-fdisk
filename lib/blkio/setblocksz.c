/*
 * blkio/setblocksz.c
 *
 * Copyright (C) 1998 Russell King
 *
 * The block I/O interface allows an OS-independent method to access devices
 *
 * Close a device on RiscOS architecture
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"

#include "blkio.h"

#include "filecore.h"

/* Function: u_int blkio_setblocksize (blkio_t *blkio, u_int blocksize)
 * Purpose : Set the block size for all subsequent block IO routines
 * Params  : blkio     - structure returned by blkio_open
 *         : blocksize - new block size to use
 * Returns : block size that will be used
 */
u_int blkio_setblocksize (blkio_t *blkio, u_int blocksize)
{
  assert (blkio != NULL);

  dbg_printf ("blkio_setblocksize(%d)", blocksize);

#ifdef RISCOS
  if (blocksize < (1 << blkio->disc_record->d.log2secsize))
    blocksize = (1 << blkio->disc_record->d.log2secsize);
#endif

  blkio->blocksize = blocksize;

  dbg_printf ("ret=%d", blocksize);

  return blocksize;
}
