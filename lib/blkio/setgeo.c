/*
 * blkio/setgeometry.c
 *
 * Copyright (C) 1998 Russell King
 *
 * The block I/O interface allows an OS-independent method to access devices
 *
 * Close a device on RiscOS architecture
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"

#include "blkio.h"

#include "filecore.h"

/* Function: u_int blkio_setgeometry (blkio_t *blkio, geometry_t *geo)
 * Purpose : Return the geometry for the device opened with blkio_open
 * Params  : blkio - structure returned by blkio_open
 *         : geo   - pointer to geometry structure
 */
u_int blkio_setgeometry (blkio_t *blkio, const geometry_t *geo)
{
  u_int log2secsize, sectors, ret = 0;

  assert (blkio != NULL);
  assert (geo != NULL);

  dbg_printf ("blkio_setgeometry([sector_size=%d, sectors=%d, heads=%d, cylinders=%d])",
  	      geo->sector_size, geo->sectors, geo->heads, geo->cylinders);
  dbg_level_up ();
  blkio->geometry = *geo;

#if defined(RISCOS)
  log2secsize = blkio->disc_record->d.log2secsize;

  if (geo->sector_size == 1 << log2secsize) {
    sectors = geo->cylinders * geo->heads * geo->sectors;
    blkio->disc_record->d.secspertrack = geo->sectors;
    blkio->disc_record->d.heads = geo->heads - (blkio->disc_record->d.lowsector & 0x40 ? 1 : 0);
    blkio->disc_record->d.disc_size_high = sectors >> (32 - log2secsize);
    blkio->disc_record->d.disc_size = sectors << log2secsize;
    dbg_printf ("-disc_record: ds=0x%08X%08X", blkio->disc_record->d.disc_size_high,
  	        blkio->disc_record->d.disc_size);
    ret = !0;
  }
#endif
#ifdef UNIX
  ret = !0;
#endif

  dbg_level_down ();
  dbg_printf ("ret=%d", ret);
  return ret;
}
