/*
 * lib/part/create.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

/* Prototype: u_int part_create(part_t *part, partinfo_t *pinfo)
 * Function : Allocate a new partition slot `parn' and kernel partition number.
 * Params   : part  - partitionable device
 *          : parn  - partition number, or PARN_ALLOCATE to use first slot
 *          : pinfo - partition info structure to set new partition as
 * Returns  : FALSE on error
 * Notes    : Do not alter any structures on disk.
 */
u_int part_allocate(part_t *part, partinfo_t *pinfo)
{
  u_int parn;

  assert(part != NULL);
  assert(part->scheme != NULL);
  assert(pinfo != NULL);

  dbg_printf("part_allocate(pinfo=[bs=0x%X, be=0x%X, type=0x%X])",
  	     pinfo->blk_start, pinfo->blk_end, pinfo->type);
  dbg_level_up();
  clear_error();

  do {
    parn = part->scheme->allocate(part, pinfo);
    if (parn == PARN_ERROR) {
      if (!is_error_set())
        set_error("unable to allocate slot for partition");
      break;
    }

    if (part->scheme->validate_partno(part, parn)) {
      if (part->nr_partitions == 0 || parn >= part->nr_partitions) {
        partinfo_i_t **p;
        u_int new_nr = parn + 1;

        p = malloc(new_nr * sizeof(*p));
        if (p) {
          memset(p, 0, new_nr * sizeof(*p));
          memcpy(p, part->partinfo, part->nr_partitions * sizeof(*p));
          free(part->partinfo);
          part->partinfo = p;
          part->nr_partitions = new_nr;
        } else {
          set_error("out of memory");
          break;
        }
      }
    } else if (!is_error_set())
      set_error("invalid partition number %d", parn);
  } while (0);

  dbg_level_down();
  dbg_printf("ret=%d (kern_part_no=%d)", parn, pinfo->kern_part_no);

  return parn;
}
