/*
 * lib/part/close.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <stdlib.h>

#include "util/debug.h"
#include "part/part.h"

/* Prototype: part_t *part_close (part_t *part)
 * Function : Close and free all memory associated with this partition device.
 * Params   : part - partitionable device to close
 * Returns  : NULL
 * Notes    : If changes are outstanding, discard the changes.
 */
part_t *part_close (part_t *part)
{
  dbg_printf ("part_close()");
  dbg_level_up ();

  if (part) {
    blkio_close (part->blkio);
    if (part->partinfo)
      free (part->partinfo);
    free (part);
  }

  dbg_level_down ();
  dbg_printf ("ret=NULL");

  return NULL;
}
