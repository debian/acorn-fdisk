/*
 * lib/part/create.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

/* Prototype: u_int part_create(part_t *part, u_int parn, const partinfo_t *pinfo)
 * Function : Create a new partition in slot `parn' described by pinfo.
 * Params   : part  - partitionable device
 *          : parn  - partition number, or PARN_ALLOCATE to use first slot
 *          : pinfo - partition info structure to set new partition as
 * Returns  : FALSE on error
 * Notes    : If it already exists, complain.  Do not alter any structures on disk.
 */
u_int part_create(part_t *part, u_int parn, const partinfo_t *pinfo)
{
  partinfo_t pnew;
  u_int ret = 0;

  assert(part != NULL);
  assert(part->scheme != NULL);
  assert(pinfo != NULL);

  pnew = *pinfo;

  dbg_printf("part_create(parn=%d, pinfo=[bs=0x%X, be=0x%X, type=0x%X])",
  	     parn, pnew.blk_start, pnew.blk_end, pnew.type);
  dbg_level_up();
  clear_error();

  do {
    if (!part_updatechs(part, &pnew))
      break;

    if (parn == PARN_ALLOCATE)
      parn = part->scheme->allocate(part, &pnew);
    if (parn == PARN_ERROR) {
      if (!is_error_set())
        set_error("unable to allocate slot for partition");
      break;
    }

    if (part->scheme->validate_partno(part, parn)) {
      if (part->nr_partitions == 0 || parn >= part->nr_partitions) {
        partinfo_i_t **p;
        u_int new_nr = parn + 1;

        p = malloc(new_nr * sizeof(*p));
        if (p) {
          memset(p, 0, new_nr * sizeof(*p));
          memcpy(p, part->partinfo, part->nr_partitions * sizeof(*p));
          free(part->partinfo);
          part->partinfo = p;
          part->nr_partitions = new_nr;
        } else {
          set_error("out of memory");
          break;
        }
      }

      if (part->scheme->validate_creation(part, parn,
		part->partinfo[parn] ? &part->partinfo[parn]->info : NULL, &pnew)) {
        if (!part->partinfo[parn])
          part->partinfo[parn] = malloc(sizeof(partinfo_i_t));
        if (part->partinfo[parn]) {
          part->partinfo[parn]->changed = 1;
          part->partinfo[parn]->info = pnew;
          ret = 1;
        }
      }
    } else if (!is_error_set())
      set_error("invalid partition number %d", parn);
  } while (0);

  dbg_level_down();
  dbg_printf("ret=%d", ret);

  return ret;
}
