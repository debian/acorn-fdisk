/*
 * lib/part/delete.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

/* Prototype: u_int part_delete (part_t *part, u_int parn)
 * Function : Delete partition `parn'.
 * Params   : part - partitionable device
 *          : parn - partition number
 * Returns  : FALSE on error
 * Notes    : Do not alter any structures on disk.
 */
u_int part_delete(part_t *part, u_int parn)
{
  u_int ret = 0;

  assert(part != NULL);
  assert(part->nr_partitions && parn < part->nr_partitions);

  dbg_printf("part_delete(%d)", parn);
  dbg_level_up();
  clear_error();

  if (part->partinfo[parn] &&
      part->scheme->validate_deletion(part, parn, &part->partinfo[parn]->info)) {
    free (part->partinfo[parn]);
    part->partinfo[parn] = NULL;
    ret = 1;
  }

  if (ret == 0 && !is_error_set())
    set_error("partition cannot be deleted");

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "error");

  return ret;
}
