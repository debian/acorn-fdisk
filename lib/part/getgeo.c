/*
 * lib/part/getgeo.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "part/part.h"

/* Prototype: u_int part_getgeometry (part_t *part, geometry_t *geo)
 * Function : Get the geometry of the drive.
 * Params   : part - partitionable device
 *          : geo  - geometry structure
 * Returns  : FALSE on error
 * Notes    : Returns blkio's idea of geometry
 */
u_int part_getgeometry (part_t *part, geometry_t *geo)
{
  u_int ret;

  assert(part != NULL);
  assert(geo != NULL);

  dbg_printf("part_getgeometry()");
  dbg_level_up();

  ret = blkio_getgeometry(part->blkio, geo);

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "error");

  return ret;
}
