/*
 * lib/part/getscheme.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "part/part.h"

/* Prototype: const char *part_getscheme (part_t *part)
 * Function : Get the partitioning scheme name.
 * Params   : part  - partitionable device
 * Returns  : pointer to a string describing scheme, or NULL
 */
const char *part_getscheme(part_t *part)
{
  assert(part != NULL);

  return part->scheme ? part->scheme->name : NULL;
}
