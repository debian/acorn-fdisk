/*
 * lib/part/open.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

static scheme_t *schemes[] = {
  &eesox_scheme,
  &icside_scheme,
  &powertec_scheme,
  &filecore_linux_scheme,
  &filecore_riscix_scheme,
  &pcbios_scheme,
  NULL
};

/* Prototype: part_t *part_open (const char *dev_name, const char *part_scheme)
 * Function : Open a device.
 * Params   : dev_name    - OS specific device name
 *          : part_scheme - partitioning scheme
 * Returns  : pointer to part_t structure
 * Notes    : Reads all partitioning data on the drive
 */
part_t *part_open(const char *dev_name, const char *part_scheme)
{
  part_t *part = NULL;

  assert(dev_name != NULL);

  dbg_printf("part_open(dev_name=%s, part_scheme=%s)", dev_name,
  	     part_scheme ? part_scheme : "");
  dbg_level_up();

  do {
    scheme_t *scheme;

    part = malloc(sizeof (part_t));
    if (!part) {
      set_error("out of memory");
      break;
    }

    memset(part, 0, sizeof (part_t));

    /*
     * open the device
     */
    part->blkio = blkio_open(dev_name);
    if (!part->blkio) {
      free(part);
      part = NULL;
      break;
    }

    /*
     * Detect partitioning scheme
     */
    if (!part_scheme) {
      scheme_t **sp;
      /* detect scheme */
      for (sp = schemes; (scheme = *sp) != NULL; sp++)
        if (scheme->detect(part))
          break;
    } else {
      scheme_t **sp;
      u_int scheme_len = strlen (part_scheme);
      u_int matches = 0;

      /* find named scheme */
      for (sp = schemes; (scheme = *sp) != NULL; sp++)
        if (strncasecmp(scheme->name, part_scheme, scheme_len) == 0) {
          matches ++;
          if (scheme->detect(part))
            break;
        }

      if (!scheme && matches == 1) {
        for (sp = schemes; (scheme = *sp) != NULL; sp++)
          if (strncasecmp(scheme->name, part_scheme, scheme_len) == 0)
            break;
      }

    }

    if (!scheme) {
      blkio_close(part->blkio);
      free(part);
      part = NULL;
      set_error("partitioning scheme not recognised");
      break;
    }

    /*
     * read partition data
     */
    if (!scheme->readinfo(part)) {
      blkio_close (part->blkio);
      free(part);
      part = NULL;
      set_error("unable to read partition tables");
      break;
    }
    part->scheme = scheme;
  } while(0);

  dbg_level_down();
  dbg_printf("ret=%p", part);

  return part;
}
