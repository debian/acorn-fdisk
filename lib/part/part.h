/*
 * lib/part/part.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef PART_PART_H
#define PART_PART_H

#include "util/types.h"
#include "blkio/blkio.h"

typedef struct {
  u_int	       cylinder;
  u_int	       head;
  u_int	       sector;
} chs_t;

typedef enum {
  /* PC compatible partition numbers */
  ptyp_none		= 0x00,  /* No defined partition		*/
  ptyp_dos12		= 0x01,	 /* DOS 12-bit FAT			*/
  ptyp_xenixroot	= 0x02,	 /* XENIX root 				*/
  ptyp_xenixusr		= 0x03,	 /* XENIX /usr				*/
  ptyp_dos16l32		= 0x04,	 /* DOS 16-bit FAT < 32MB		*/
  ptyp_dos_extended	= 0x05,	 /* DOS Extended partition		*/
  ptyp_dos16g32		= 0x06,	 /* DOS 16-bit FAT >= 32MB		*/
  ptyp_hpfs		= 0x07,	 /* HPFS				*/
  ptyp_aix		= 0x08,	 /* AIX					*/
  ptyp_aixboot		= 0x09,	 /* AIX boot				*/
  ptyp_os2		= 0x0a,	 /* OS/2				*/
  ptyp_win98_extended	= 0x0f,	 /* Win98 extended partition		*/
  ptyp_venix		= 0x40,	 /* VENIX				*/
  ptyp_novell		= 0x51,	 /* Novell				*/
  ptyp_dm6_aux1		= 0x51,	 /* Disk Manager			*/
  ptyp_microport	= 0x52,	 /* MicroPort				*/
  ptyp_dm6_aux3		= 0x53,	 /* Disk Manager			*/
  ptyp_dm6		= 0x54,	 /* Disk Manager			*/
  ptyp_ezdrive		= 0x55,	 /* EZ-Drive				*/
  ptyp_gnuhurd		= 0x63,	 /* GNU HURD				*/
  ptyp_novelnet286	= 0x64,	 /* Novel Net 286			*/
  ptyp_novelnet386	= 0x65,	 /* Novel Net 386			*/
  ptyp_pcix		= 0x75,	 /* PCIX				*/
  ptyp_old_minix	= 0x80,	 /* Old MINIX partition			*/
  ptyp_linux_parttion	= 0x80,	 /* Old Linux partition			*/
  ptyp_minix		= 0x81,  /* MINIX partition			*/
  ptyp_linux_swap	= 0x82,  /* Linux swap				*/
  ptyp_linux_native	= 0x83,  /* Linux native			*/
  ptyp_linux_extended	= 0x85,	 /* Linux Extended			*/
  ptyp_amoeba		= 0x93,	 /* AMOEBA				*/
  ptyp_amoebabbt	= 0x94,	 /* AMOEBA bad block table		*/
  ptyp_bsd386		= 0xa5,	 /* BSD 386				*/
  ptyp_bsdifs		= 0xb7,	 /* BSDi FS				*/
  ptyp_bsdiswap		= 0xb8,	 /* BSDi Swap				*/
  ptyp_syrinx		= 0xc7,	 /* Syrinx				*/
  ptyp_cpm		= 0xdb,	 /* CP/M				*/
  ptyp_dosaccess	= 0xe1,	 /* DOS Access				*/
  ptyp_dosro		= 0xe3,	 /* DOS Read/Only			*/
  ptyp_dosscc		= 0xf2,	 /* DOS SCC				*/
  ptyp_bbt		= 0xff,	 /* Bad block table			*/
  /* Other partition types */
  ptyp_filecore		= 0x100, /* RiscOS filecore partition		*/
  ptyp_linux_table	= 0x101, /* RiscOS filecore Linux partition tbl	*/
  ptyp_pc_table		= 0x102, /* PC partition table			*/
  ptyp_pt_oldmap	= 0x103, /* for PowerTec OLDMAP partitions	*/
  ptyp_pt_backup	= 0x104, /* for PowerTec BACKUP partitions	*/
  ptyp_pt_empty		= 0x105, /* for PowerTec EMPTY partitions 	*/
  ptyp_reserved		= 0xfffe,/* Space used/reserved                 */
  ptyp_unknown		= 0xffff /* unrecognised partition		*/
} ptyp_t;

/* External data structure
 */
typedef struct {
  bool_t	chs_valid;	/* CHS address is valid		*/
  chs_t		chs_start;	/* For PC BIOS-type schemes	*/
  chs_t		chs_end;
  blk_t		blk_start;	/* For LBA block addresses	*/
  blk_t		blk_end;
  ptyp_t	type;		/* Partition type		*/
  u_int		kern_part_no;	/* Kernel partition number	*/
} partinfo_t;

#include "part/schemedata.h"

typedef struct {
  bool_t	changed;	/* change flag			*/
  partinfo_t	info;		/* partition information	*/
  scheme_part_t	data;		/* scheme-specific data		*/
} partinfo_i_t;

/* Internal data structure: No data should be used from this structure
 * in the application layer.
 */
typedef struct {
  blkio_t	*blkio;		/* Device			*/
  struct scheme	*scheme;	/* Scheme			*/
  scheme_data_t	data;		/* Scheme-specific data		*/
  u_int		nr_partitions;	/* Number of partitions		*/
  partinfo_i_t	**partinfo;	/* Partition info		*/
} part_t;

#include "part/scheme.h"

/*
 * Use this parn to tell part_create to allocate a partition number
 */
#define PARN_ALLOCATE		(-1U)
#define PARN_ERROR		(-2U)

#define VALIDOPS_UPDATE		(1 << 0)
#define VALIDOPS_DELETE		(1 << 1)

/* Prototype: u_int part_create(part_t *part, partinfo_t *pinfo)
 * Function : Allocate a new partition slot `parn' and kernel partition number.
 * Params   : part  - partitionable device
 *          : parn  - partition number, or PARN_ALLOCATE to use first slot
 *          : pinfo - partition info structure to set new partition as
 * Returns  : FALSE on error
 * Notes    : Do not alter any structures on disk.
 */
u_int part_allocate(part_t *part, partinfo_t *pinfo);

/* Prototype: part_t *part_close (part_t *part)
 * Function : Close and free all memory associated with this partition device.
 * Params   : part - partitionable device to close
 * Returns  : NULL
 * Notes    : If changes are outstanding, discard the changes.
 */
extern part_t *part_close(part_t *part);

/* Prototype: u_int part_create (part_t *part, u_int parn, const partinfo_t *pinfo)
 * Function : Create a new partition in slot `parn' described by pinfo.
 * Params   : part  - partitionable device
 *          : parn  - partition number
 *          : pinfo - partition info structure to set new partition as
 * Returns  : FALSE on error
 * Notes    : If it already exists, complain.  Do not alter any structures on disk.
 */
extern u_int part_create(part_t *part, u_int parn, const partinfo_t *pinfo);

/* Prototype: u_int part_delete (part_t *part, u_int parn)
 * Function : Delete partition `parn'.
 * Params   : part - partitionable device
 *          : parn - partition number
 * Returns  : FALSE on error
 * Notes    : Do not alter any structures on disk.
 */
extern u_int part_delete(part_t *part, u_int parn);

/* Prototype: u_int part_getgeometry (part_t *part, geometry_t *geo)
 * Function : Get the geometry of the drive.
 * Params   : part - partitionable device
 *          : geo  - geometry structure
 * Returns  : FALSE on error
 * Notes    : Returns blkio's idea of geometry
 */
extern u_int part_getgeometry(part_t *part, geometry_t *geo);

/* Prototype: u_int part_getpartinfo (part_t *part, u_int parn, partinfo_t *pinfo)
 * Function : Get the partition details for partition `parn'.
 * Params   : part  - partitionable device
 *          : parn  - partition number
 *          : pinfo - partition info structure to fill out
 * Returns  : FALSE on error
 */
extern u_int part_getpartinfo(part_t *part, u_int parn, partinfo_t *pinfo);

/* Prototype: const char *part_getscheme (part_t *part)
 * Function : Get the partitioning scheme name.
 * Params   : part  - partitionable device
 * Returns  : pointer to a string describing scheme, or NULL
 */
extern const char *part_getscheme(part_t *part);

/* Prototype: ptyp_t part_nexttype(part_t *part, u_int parn, ptyp_t current, int dir)
 * Purpose  : Return the next partition type
 * Params   : part    - partiionable device
 *          : parn    - partition number
 *          : current - current partition type
 *          : dir     - increment (+1), or decrement (-1)
 * Returns  : next partition type
 */
extern ptyp_t part_nexttype(part_t *part, u_int parn, ptyp_t current, int dir);

/* Prototype: part_t *part_open (const char *dev_name, const char *part_scheme)
 * Function : Open a device.
 * Params   : dev_name    - OS specific device name
 *          : part_scheme - partitioning scheme
 * Returns  : pointer to part_t structure
 * Notes    : Reads all partitioning data on the drive
 */
extern part_t *part_open(const char *dev_name, const char *part_scheme);

/* Prototype: u_int part_read (part_t *part, u_int parn, void *data, blk_t blk, u_int nr_blks)
 * Function : Read blocks from a partition
 * Params   : part    - partitionable device
 *          : parn    - partition number
 *          : data    - data area to read into
 *          : blk     - starting block
 *          : nr_blks - number of blocks to read
 * Returns  : actual number of blocks read
 */
extern u_int part_read(part_t *part, u_int parn, void *data, blk_t blk, u_int nr_blks);

/* Prototype: u_int part_setgeometry (part_t *part, const geometry_t *geo)
 * Function : Set the geometry of the drive
 * Params   : part - partitionable device
 *          : geo  - geometry
 * Returns  : FALSE on error
 * Notes    : This does not set the geometry that the OS' idea of the geometry.
 */
extern u_int part_setgeometry(part_t *part, const geometry_t *geo);

/* Prototype: u_int part_setpartinfo(part_t *part, u_int parn, const partinfo_t *pinfo)
 * Function : Set partition details for partition
 * Params   : part  - partitionable device
 *          : parn  - partition number
 *          : pinfo - partition info structure to set partition
 * Returns  : FALSE on error
 */
extern u_int part_setpartinfo(part_t *part, u_int parn, const partinfo_t *pinfo);

/* Prototype: u_int part_sync (part_t *part)
 * Function : Synchronise (write) partition information with disk.
 * Params   : part - partitionable device
 * Returns  : FALSE on error
 */
extern u_int part_sync(part_t *part);

/* Prototype: const char *part_typename(part_t *part, ptyp_t type)
 * Purpose  : Return the name of the partition type 'type'
 * Params   : part - partitionable device
 *          : type - partition type
 * Returns  : const pointer to NUL terminated string
 */
extern const char *part_typename(part_t *part, ptyp_t type);

/* Prototype: u_int part_updatechs(part_t *part, partinfo_t *pinfo)
 * Purpose  : Update CHS values if they have become invalid
 * Params   : part  - partitionable device
 *          : pinfo - partition to update
 * Returns  : FALSE on error
 */
u_int part_updatechs(part_t *part, partinfo_t *pinfo);

/* Prototype: u_int part_validops (part_t *part, u_int parn, const partinfo_t *pinfo)
 * Function : Returns a bitmask containing the valid operations that can be performed
 *            on the specified partition
 * Params   : part - partitionable device
 *          : parn - partition number
 * Returns  : bitmask of valid operations
 */
extern u_int part_validops(part_t *part, u_int parn, const partinfo_t *pinfo);

/* Prototype: u_int part_write (part_t *part, u_int parn, const void *data, blk_t blk, u_int nr_blks)
 * Function : Write blocks to a partition
 * Params   : part    - partitionable device
 *          : parn    - parititon number
 *          : data    - data to write
 *          : blk     - starting block
 *          : nr_blks - number of blocks
 * Returns  : FALSE on error
 */
extern u_int part_write(part_t *part, u_int parn, const void *data, blk_t blk, u_int nr_blks);

#endif
