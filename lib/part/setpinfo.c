/*
 * lib/part/setpinfo.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

/* Prototype: u_int part_setpartinfo (part_t *part, u_int parn, const partinfo_t *pinfo)
 * Function : Set partition details for partition
 * Params   : part  - partitionable device
 *          : parn  - partition number
 *          : pinfo - partition info structure to set partition
 * Returns  : FALSE on error
 */
u_int part_setpartinfo(part_t *part, u_int parn, const partinfo_t *pinfo)
{
  partinfo_t pnew;
  u_int ret = 0;

  assert(part != NULL);
  assert(part->nr_partitions && parn < part->nr_partitions);
  assert(pinfo != NULL);
  assert(part->scheme);

  pnew = *pinfo;

  dbg_printf("part_setpartinfo(parn=%d, pinfo=[bs=0x%X, be=0x%X, type=0x%X])",
             parn, pinfo->blk_start, pinfo->blk_end, pinfo->type);
  dbg_level_up();

  do {
    if (!part_updatechs(part, &pnew))
      break;

    if (part->partinfo[parn]) {
      ret = part->scheme->validate_change(part, parn, &part->partinfo[parn]->info, &pnew);
      if (ret) {
        part->partinfo[parn]->info = pnew;
        part->partinfo[parn]->changed = 1;
      }
    } else
      set_error("no partition defined for partition %d", parn);
  } while (0);

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "error");

  return ret;
}
