/*
 * lib/part/types.c
 *
 * Copyright (C) 1998 Russell King
 */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "part/part.h"

static const struct {
  ptyp_t type;
  const char *name;
} part_types[] = {
  /*
   * If a partition type is not listed in this
   * table, then the following function will
   * assert.  Keep this table up to date!
   */
  { ptyp_none,		"None"			},
  { ptyp_dos12,		"DOS-12"		},
  { ptyp_xenixroot,	"XENIX root"		},
  { ptyp_xenixusr,	"XENIX /usr"		},
  { ptyp_dos16l32,	"DOS-16 < 32MB"		},
  { ptyp_dos_extended,	"DOS Extended"		},
  { ptyp_dos16g32,	"DOS-16 >= 32MB"	},
  { ptyp_hpfs,		"HPFS"	    		},
  { ptyp_aix,		"AIX"			},
  { ptyp_aixboot,	"AIX Boot"		},
  { ptyp_os2,		"OS/2"			},
  { ptyp_win98_extended,"Win98 Extended"	},
  { ptyp_venix,		"VENIX"			},
  { ptyp_novell,	"Novel"			},
  { ptyp_dm6_aux1,	"DM6 Aux1"		},
  { ptyp_microport,	"MicroPort"		},
  { ptyp_dm6_aux3,	"DM6 Aux3"		},
  { ptyp_dm6,		"Disk Manager 6"	},
  { ptyp_ezdrive,	"EZ-Drive"    		},
  { ptyp_gnuhurd,	"GNU Hurd"		},
  { ptyp_novelnet286,	"NovelNet 286"		},
  { ptyp_novelnet386,	"NovelNet 386"		},
  { ptyp_pcix,		"PCiX"	  		},
  { ptyp_old_minix,	"Old MINIX"		},
  { ptyp_minix,		"Minix"			},
  { ptyp_linux_swap,	"Linux Swap"		},
  { ptyp_linux_native,	"Linux Native"		},
  { ptyp_linux_extended,"Linux Extended"	},
  { ptyp_amoeba,	"AMOEBA"		},
  { ptyp_amoebabbt,	"AMOEBA BBT"		},
  { ptyp_bsd386,	"BSD 386"		},
  { ptyp_bsdifs,	"BSDi FS"		},
  { ptyp_bsdiswap,	"BSDi Swap"		},
  { ptyp_syrinx,	"Syrinx"		},
  { ptyp_cpm,		"CP/M"			},
  { ptyp_dosaccess,	"DOS Access"		},
  { ptyp_dosro,		"DOS R/O"		},
  { ptyp_dosscc,	"DOS SCC"		},
  { ptyp_bbt,		"Bad Block Table"	},
  { ptyp_filecore,	"Filecore"		},
  { ptyp_linux_table,	"Linux Table"		},
  { ptyp_pc_table,	"BIOS Table"		},
  { ptyp_pt_oldmap,	"Filecore OldMap"	},
  { ptyp_pt_backup,	"PowerTec Backup"	},
  { ptyp_pt_empty,	"Empty"			},
  { ptyp_unknown,	"Unknown"		},
  { ptyp_none,		NULL			}
};

/* Prototype: const char *part_typename(part_t *part, ptyp_t type)
 * Purpose  : Return the name of the partition type 'type'
 * Params   : part - partitionable device
 *          : type - partition type
 * Returns  : const pointer to NUL terminated string
 */
const char *part_typename(part_t *part, ptyp_t type)
{
  static char buf[32];
  const char *name;
  u_int i;

  for (i = 0; part_types[i].name; i++)
    if (part_types[i].type == type)
      break;

  if (part_types[i].name)
    name = part_types[i].name;
  else {
    sprintf(buf, "Unknown type %d", type);
    name = buf;
  }

  return name;
}

/* Prototype: ptyp_t part_nexttype(part_t *part, u_int parn, ptyp_t current, int dir)
 * Purpose  : Return the next partition type
 * Params   : part    - partiionable device
 *          : parn    - partition number
 *          : current - current partition type
 *          : dir     - increment (+1), or decrement (-1)
 * Returns  : next partition type
 */
ptyp_t part_nexttype(part_t *part, u_int parn, ptyp_t current, int dir)
{
  static ptyp_t types[] = { ptyp_linux_swap, ptyp_linux_native, ptyp_filecore };
  ptyp_t ptype;
  int i;
#define NR_TYPES (sizeof(types)/sizeof(ptyp_t))

  if (part->scheme->nexttype)
    ptype = part->scheme->nexttype(part, parn, current, dir);
  else {
    for(i = 0; i < NR_TYPES; i++)
      if (types[i] == current)
        break;

    if (types[i] == current) {
      i += dir;

      if (i < 0)
        i = 0;

      if (i >= NR_TYPES)
       i = NR_TYPES - 1;
    }

    ptype = types[i];
  }

  return ptype;
}
