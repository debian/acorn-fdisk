/*
 * lib/part/utils.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef PART_UTILS_H
#define PART_UTILS_H

#include "part/part.h"

/* Function: u_int part_add (part_t *part, u_int parn, partinfo_t *pinfo)
 * Purpose : add a new partition (as read from disk) to the partition structures
 * Params  : part  - partitionable device
 *         : parn  - partition number
 *         : pinfo - partition info
 * Returns : FALSE on error
 */
extern u_int part_add (part_t *part, u_int parn, partinfo_i_t *pinfo);

/* Function: u_int check_overlap(part_t *part, u_int exclude_parn, partinfo_t *pinfo)
 * Purpose : check that the new partition information `pinfo' for `exclude_parn' entry
 *           does not overlap physically on the disk with the existing partitions.
 * Params  : part         - partitionable device
 *         : exclude_parn - partition entry to exclude from check
 *         : pinfo        - new partition information to check against
 * Returns : TRUE if new information overlaps existing
 */
extern u_int check_overlap(part_t *part, u_int exclude_parn, const partinfo_t *pinfo);
#endif
