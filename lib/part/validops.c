/*
 * lib/part/validops.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "part/part.h"

/* Prototype: u_int part_validops (part_t *part, u_int parn, const partinfo_t *pinfo)
 * Function : Returns a bitmask containing the valid operations that can be performed
 *            on the specified partition
 * Params   : part - partitionable device
 *          : parn - partition number
 * Returns  : bitmask of valid operations
 */
u_int part_validops(part_t *part, u_int parn, const partinfo_t *pinfo)
{
  u_int ret = 0;

  assert(part != NULL);
  assert(part->nr_partitions && parn < part->nr_partitions);
  assert(part->scheme);

  dbg_printf("part_validops(parn=%d)", parn);
  dbg_level_up();

  if (part->partinfo[parn]) {
    if (part->scheme->validate_change(part, parn, &part->partinfo[parn]->info, pinfo))
      ret |= VALIDOPS_UPDATE;
    if (part->scheme->validate_deletion(part, parn, &part->partinfo[parn]->info))
      ret |= VALIDOPS_DELETE;
  }

  dbg_level_down();
  dbg_printf("ret %X", ret);

  return ret;
}
