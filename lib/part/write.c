/*
 * lib/part/write.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "part/part.h"

/* Prototype: u_int part_write (part_t *part, u_int parn, const void *data, blk_t blk, u_int nr_blks)
 * Function : Write blocks to a partition
 * Params   : part    - partitionable device
 *          : parn    - parititon number
 *          : data    - data to write
 *          : blk     - starting block
 *          : nr_blks - number of blocks
 * Returns  : actual number of blocks read
 */
u_int part_write(part_t *part, u_int parn, const void *data, blk_t blk, u_int nr_blks)
{
  partinfo_i_t *pinfo;
  u_int ret = 0;

  assert(part != NULL);
  assert(part->nr_partitions && parn < part->nr_partitions);
  assert(data != NULL);

  dbg_printf("part_write(parn=%d, data=%p, blk=0x%X, nr=0x%X)", parn, data, blk, nr_blks);
  dbg_level_up();

  pinfo = part->partinfo[parn];

  if (pinfo) {
    blk += pinfo->info.blk_start;
    if (nr_blks <= (pinfo->info.blk_end - pinfo->info.blk_start) &&
        blk < (pinfo->info.blk_end - nr_blks))
      ret = blkio_write(part->blkio, data, blk, nr_blks);
  }

  dbg_level_down();
  dbg_printf("ret=%d", ret);

  return ret;
}
