#include <stdio.h>

#include "part/part.h"
#include "util/error.h"

int main(int argc, char *argv[])
{
  partinfo_t info;
  geometry_t geo;
  part_t *part;
  int i;

  part = part_open(argv[1], "");
  if (!part) {
    fprintf(stderr, "Unable to open device: %s\n", get_error());
    exit(1);
  }

  part_getgeometry(part, &geo);

  printf("Drive: %d cylinders %d heads %d sectors\n\n",
  	geo.cylinders, geo.heads, geo.sectors);
  printf("Partitioning scheme is: %s\n\n", part_getscheme(part));

  i = 0;
  printf ("Partition | Flgs | Start    C   /H /S  | End      C   /H /S  | Type\n");
  printf ("----------+------+---------------------+---------------------+-----------------\n");
  while (part_getpartinfo(part, i, &info)) {
  	int ops;

  	ops = part_validops(part, i, &info);

  	printf ("%8s%-2d| %c%c   | %08X %4d/%2d/%2d | %08X %4d/%2d/%2d | %03X %s\n",
  		argv[1],
		info.kern_part_no,
		ops & VALIDOPS_DELETE ? 'D' : ' ',
		ops & VALIDOPS_UPDATE ? 'E' : ' ',
		info.blk_start,
		info.chs_start.cylinder,
		info.chs_start.head,
		info.chs_start.sector,
		info.blk_end,
		info.chs_end.cylinder,
		info.chs_end.head,
		info.chs_end.sector,
		info.type,
		part_typename(part, info.type));
  	i++;
  }
  printf ("----------+------+---------------------+---------------------+-----------------\n");

  part_close(part);
  return 0;
}
