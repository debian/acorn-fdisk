/*
 * lib/scheme/eesox.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_EESOX_H
#define SCHEME_EESOX_H

#define SCHEME_EESOX

typedef struct {
  u_int unused;
} eesox_data_t;

typedef struct {
  u_int idx;	/* offset into partition table			*/
} eesox_part_t;

extern struct scheme eesox_scheme;

#endif
