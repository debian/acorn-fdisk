/*
 * lib/scheme/filecore.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "scheme/filecore.h"

u_int filecore_checksum(u8 *sector)
{
  u_int sum = 0;
  int i;

  assert(sector != NULL);

  for (i = 510; i >= 0; i--)
    sum = (sum & 255) + sector[i] + (sum >> 8);
  return sum & 255;
}
