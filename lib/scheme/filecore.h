/*
 * lib/scheme/filecore.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_FILECORE_H
#define SCHEME_FILECORE_H

#include "util/types.h"

/* Function: u_int filecore_checksum (unsigned char *sector)
 * Purpose : Calculate a filecore checksum
 * Params  : sector - filecore sector to checksum
 * Returns : filecore checksum
 */
extern u_int filecore_checksum (u8 *sector);

#endif
