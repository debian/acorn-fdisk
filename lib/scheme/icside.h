/*
 * lib/scheme/icside.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_ICSIDE_H
#define SCHEME_ICSIDE_H

#define SCHEME_ICSIDE

typedef struct {
  u_int unused;
} icside_data_t;

typedef struct {
  u_int idx;		/* index into partition table */
} icside_part_t;

extern struct scheme icside_scheme;

#endif
