/*
 * lib/scheme/linux.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_LINUX_H
#define SCHEME_LINUX_H

#include "blkio/filecore.h"

#define SCHEME_FILECORE_LINUX

typedef struct {
  disc_record_t disc_record;	/* disc record			*/
  u8		boot_checksum;	/* checksum of boot sector	*/
} filecore_linux_data_t;

typedef struct {
  u_int part_offset;		/* offset into partition table	*/
} filecore_linux_part_t;

extern struct scheme filecore_linux_scheme;

#endif
