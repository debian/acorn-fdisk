/*
 * lib/scheme/pcbios.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_PCBIOS_H
#define SCHEME_PCBIOS_H

#define SCHEME_PCBIOS

typedef struct {
  u8 *pcboot_sector;
  u8 *extended_sector;
} pcbios_data_t;

typedef struct {
  u_int part_offset;		/* offset into partition table */
} pcbios_part_t;

extern struct scheme pcbios_scheme;

#endif
