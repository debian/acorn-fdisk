/*
 * lib/scheme/powertec.c
 *
 * Partition backend for PowerTec SCSI partitioning scheme on the
 * Acorn machines.
 *
 * Copyright (C) 1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "util/error.h"
#include "util/types.h"
#include "part/part.h"
#include "part/utils.h"

#define POWERTEC_SECTOR_SIZE	512
#define POWERTEC_PART_SECTOR	0
#define POWERTEC_NR_PARTITIONS	16

#undef  POWERTEC_DUMP
#define POWERTEC_SAVE

/* 24 -> 6 words
 *                   - start- - size -          ----- name ------
 * 00000000 00000000 00000000 00080000 00000000 63736952 003a734f
 * 00000000 00000000 00080070 00000000 00000000 63736952 003a734f
 * 00000000 00000000 00080070 00000000 00000000 63736952 003a734f
 * ... etc ...
 * 00000000 00000000 00000000 00053000 00000000 63736952 003a734f
 * 00000000 00000000 00053020 0002cfe0 00000000 63736952 003a734f
 * 00000000 00000000 00080070 00000000 00000000 63736952 003a734f
 * ... etc ...
 */
typedef struct {
  u32 unused1;
  u32 unused2;
  u32 start;
  u32 size;
  u32 unused5;
  char type[8];
} pe_t;

typedef union {
  pe_t pe[POWERTEC_SECTOR_SIZE / sizeof(pe_t)];
  u8 sector[POWERTEC_SECTOR_SIZE];
} powertec_psect_t;

typedef struct {
  const ptyp_t type;
  const u_int  len;
  const char   name[9];
} powertec_ptyp_t;

/*
 * This is the list of partition types that we recognise -
 *  we use this table to convert ptyp_ to name and vice versa
 */
static powertec_ptyp_t powertec_ptypes[] = {
  { ptyp_linux_swap,   8, "LinuxS:"  },
  { ptyp_linux_native, 8, "LinuxN:"  },
  { ptyp_filecore,     8, "RiscOs:"  },
  { ptyp_pt_oldmap,    8, "OldMap:"  },
  { ptyp_pt_backup,    8, "Backup:"  },
  { ptyp_pt_empty,     7, "Empty:"   },
  { ptyp_dos16g32,     8, "DOSDisc:" },
  { ptyp_unknown,      0, ""         }
};

#define NR_TYPES (sizeof(powertec_ptypes) / sizeof(powertec_ptyp_t))

/* Prototype: char *powertec_getstr(pe_t *pe)
 * Purpose  : Read the type string from the partition table
 * Params   : pe - pointer to powertec partition entty
 * Returns  : pointer to NUL terminated string
 */
static char *
powertec_getstr(pe_t *pe)
{
  u_int len;
  char *str = NULL;

  for (len = 0; len < sizeof(pe->type); len++)
    if (pe->type[len] == '\0')
      break;

  if (len) {
    str = malloc(len + 1);
    if (str) {
      memcpy(str, pe->type, len);
      str[len] = '\0';
    }
  }
  return str;
}

/* Prototype: void powertec_putstr(pe_t *pe, char *str)
 * Purpose  : Write the type string to the partition table
 * Params   : pe  - pointer to powertec partition entty
 *          : str - pointer to string
 */
static void
powertec_putstr(pe_t *pe, char *str)
{
  memcpy(pe->type, str, strlen(str));
}

/* Prototype: ptyp_t powertec_gettype(pe_t *pe)
 * Purpose  : Convert a powertec partition type to our internal
 *            partition type numbers
 * Params   : pe - pointer to powertec partition entry
 * Returns  : our internal ptyp_ number, or ptyp_unknown if not recognised
 */
static ptyp_t
powertec_gettype(pe_t *pe)
{
  u_int i;

  /*
   * If we don't recognise the type, we will be set at the final entry
   */
  for (i = 0; i < NR_TYPES - 1; i++)
    if (memcmp(pe->type, powertec_ptypes[i].name, powertec_ptypes[i].len) == 0)
      break;

  return powertec_ptypes[i].type;
}

/* Prototype: void powertec_puttype(pe_t *pe, ptyp_t type)
 * Purpose  : Convert our internal partition numbers to a
 *            powertec partition type name
 * Params   : pe   - pointer to powertec partition entry
 *          : type - our internal partition type number
 * Notes    : if we are unable to store our partition type number,
 *            we will assert.  This should never happen.
 */
static void
powertec_puttype(pe_t *pe, ptyp_t type)
{
  u_int i;

  for (i = 0; i < NR_TYPES - 1; i++)
    if (powertec_ptypes[i].type == type)
      break;

  if (i >= NR_TYPES)
    assert(0);

  memset(pe->type, 0, sizeof (pe->type));
  memcpy(pe->type, powertec_ptypes[i].name, powertec_ptypes[i].len);
}

/* Prototype: u_int powertec_checksum(u_char *sector)
 * Purpose  : generate a PowerTec SCSI checksum
 * Params   : sector - sector comtaining partition information
 * Returns  : calculated checksum
 */
static u_int
powertec_checksum(u_char *sector)
{
  u_int sum = 0x2a;
  int i;

  for (i = 0; i < 511; i++)
    sum += sector[i];

  return sum & 255;
}

/* Prototype: u_int powertec_detect(part_t *part)
 * Function : detect a drive partitioned with an ICS IDE table
 * Params   : part - partitionable device
 * Returns  : FALSE if not detected
 */
static u_int
powertec_detect(part_t *part)
{
  powertec_psect_t sector;
  u_int ret = 0;
  dbg_printf("powertec_detect()");
  dbg_level_up();

  assert(part != NULL);

  do {
    u_int disc_csum, calc_csum;
    if (blkio_setblocksize(part->blkio, POWERTEC_SECTOR_SIZE) != POWERTEC_SECTOR_SIZE)
      break;

    if (blkio_read(part->blkio, sector.sector, POWERTEC_PART_SECTOR, 1) != 1)
      break;

    disc_csum = sector.sector[511];
    calc_csum = powertec_checksum(sector.sector);
    dbg_printf("-powertec disc checksum: %02X", disc_csum);
    dbg_printf("-powertec calc checksum: %02X", calc_csum);
    if (calc_csum != disc_csum)
      break;

    ret = 1;
  } while (0);

  dbg_level_down();
  dbg_printf("ret%s detected", ret ? "" : " not");
  return ret;
}

/* Prototype: u_int powertec_readinfo(part_t *part)
 * Function : read all partition information from the drive
 * Params   : part - partitionable device
 * Returns  : FALSE on error
 */
static u_int
powertec_readinfo(part_t *part)
{
  u_int ret = 0;

  dbg_printf("powertec_readinfo()");
  dbg_level_up();

  assert(part != NULL);

  do {
    powertec_psect_t sector;
    partinfo_i_t pinfo;
    u_int part_no;

    if (blkio_read(part->blkio, sector.sector, POWERTEC_PART_SECTOR, 1) != 1)
      break;

    ret = 1;

    for (part_no = 0; part_no < POWERTEC_NR_PARTITIONS; part_no++)
      if (sector.pe[part_no].size != 0) {
        pinfo.info.chs_valid     = 0;
        pinfo.info.blk_start	 = sector.pe[part_no].start;
        pinfo.info.blk_end	 = sector.pe[part_no].start + sector.pe[part_no].size - 1;
        pinfo.info.kern_part_no	 = part_no + 1;
        pinfo.data.powertec.idx  = part_no;
        pinfo.data.powertec.type = powertec_getstr(&sector.pe[part_no]);
        pinfo.info.type = powertec_gettype(&sector.pe[part_no]);

        if (!part_add(part, part_no + 1, &pinfo)) {
          ret = 0;
          break;
        }

        /* should we check the filecore boot sector? */
      }
  } while (0);

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "error");

  return ret;
}

/* Prototype: u_int powertec_writeinfo(part_t *part)
 * Function : write all partition information back to the drive
 * Params   : part - partitionable device
 * Returns  : FALSE on error
 */
static u_int
powertec_writeinfo(part_t *part)
{
  powertec_psect_t sector;
  u_int part_no, ret = 0;

  dbg_printf("powertec_writeinfo()");
  dbg_level_up();

  assert(part != NULL);
  assert(part->nr_partitions <= POWERTEC_NR_PARTITIONS);

  memset(sector.sector, 0, sizeof(sector.sector));

  for (part_no = 0; part_no < part->nr_partitions; part_no++)
    if (part->partinfo[part_no] && part->partinfo[part_no]->info.type != ptyp_none) {
      partinfo_i_t *info = part->partinfo[part_no];
      u_int idx = info->data.powertec.idx;
      char *str = info->data.powertec.type;

      sector.pe[idx].start = info->info.blk_start;
      sector.pe[idx].size  = info->info.blk_end - info->info.blk_start + 1;
      if (info->info.type != ptyp_unknown)
        powertec_puttype(&sector.pe[idx], info->info.type);
      else
        powertec_putstr(&sector.pe[idx], str);
    }

  sector.sector[254] = 1; /* what does this do? */
  sector.sector[511] = powertec_checksum(sector.sector);
#ifdef POWERTEC_DUMP
  dbg_memdump(sector.sector, POWERTEC_SECTOR_SIZE);
#endif
#ifdef POWERTEC_SAVE
  if (blkio_write(part->blkio, sector.sector, POWERTEC_PART_SECTOR, 1) != 1)
    set_error("unable to write partition sector");
  else
    ret = 1;
#else
  set_error("PowerTec partition information cannot be saved with this build");
#endif

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "error");

  return ret;
}

/* Prototype: u_int powertec_allocate(part_t *part, partinfo_t *pnew)
 * Function : allocate a partition entry number and a kernel
 *            partition number for a new partition
 * Params   : part - partitionable device
 * Returns  : partition number, or PARN_ERROR on error
 */
static u_int
powertec_allocate(part_t *part, partinfo_t *pnew)
{
  u_int parn, test;
  assert(part != NULL);

  dbg_printf("powertec_allocate()");
  dbg_level_up();

  /*
   * Find a free slot in the partition table
   */
  for (test = 1, parn = 1; parn < part->nr_partitions; parn++)
    if (part->partinfo[parn] && part->partinfo[parn]->data.powertec.idx == test) {
      test += 1;
      parn = 1;
    }

  /*
   * Look for a free entry in our structures
   */
  for (parn = 1; parn < part->nr_partitions; parn++)
    if (!part->partinfo[parn])
      break;

  if (parn > POWERTEC_NR_PARTITIONS || test > POWERTEC_NR_PARTITIONS) {
    set_error("partition table is full");
    parn = PARN_ERROR;
  } else {
    pnew->kern_part_no = test;
    pnew->type = ptyp_linux_native;
  }

  dbg_level_down();
  dbg_printf("ret %d", parn);

  return PARN_ERROR;
}

/* Prototype: u_int powertec_validate_change(part_t *part, u_int parn,
 *                                         const partinfo_t *pold, const partinfo_t *pnew)
 * Function : validate changes made to a partition
 * Params   : part - partitionable device
 *          : parn - partition number
 *          : pold - old partition information
 *          : pnew - new partition information
 * Returns  : FALSE if we reject the change
 */
static u_int
powertec_validate_change(part_t *part, u_int parn, const partinfo_t *pold, const partinfo_t *pnew)
{
  u_int ret = 0;

  dbg_printf("powertec_validate_change(parn=%d)", parn);
  dbg_level_up();

  assert(part != NULL);
  assert(pold != NULL);
  assert(parn < part->nr_partitions);

  do {
    if (pold->type == ptyp_filecore) {
      set_error("you cannot edit a filecore partition");
      break;
    }

    if (pnew) {
      if (pnew->type == ptyp_filecore) {
        set_error("you cannot create a filecore partition");
        break;
      }

      if (check_overlap(part, parn, pnew)) {
        set_error("the modified partition overlaps an existing partition");
        break;
      }
    }
    ret = 1;
  } while (0);

  dbg_level_down();
  dbg_printf("ret %svalid", ret ? "" : "in");

  return ret;
}

/* Prototype: u_int powertec_validate_creation(part_t *part, u_int parn,
 *                                           const partinfo_t *pold, const partinfo_t *pnew)
 * Function : validate changes made to a partition
 * Params   : part - partitionable device
 *          : parn - partition number
 *          : pold - old partition information (NULL if none)
 *          : pnew - new partition information
 * Returns  : FALSE if we reject the creation
 */
static u_int
powertec_validate_creation(part_t *part, u_int parn, const partinfo_t *pold, const partinfo_t *pnew)
{
  u_int ret = 0;

  dbg_printf("powertec_validate_creation(parn=%d)", parn);
  dbg_level_up();

  assert(part != NULL);
  assert(parn < part->nr_partitions);

  do {
    if (pold != NULL) {
      set_error("unable to create partition in this slot");
      break;
    }

    if (pnew) {
      if (pnew->type == ptyp_filecore) {
        set_error("you cannot create a filecore partition");
        break;
      }

      if (check_overlap(part, parn, pnew)) {
        set_error("the new partition overlaps an existing partition");
        break;
      }
    }

    ret = 1;
  } while (0);

  dbg_level_down();
  dbg_printf("ret %svalid", ret ? "" : "in");

  return ret;
}

/* Prototype: u_int powertec_validate_deletion(part_t *part, u_int parn, const partinfo_t *pold)
 * Function : validate a deletion of a partition
 * Params   : part - partitionable device
 *          : parn - partition number
 *          : pold - old partition information
 * Returns  : FALSE if we reject the deletion
 */
static u_int
powertec_validate_deletion(part_t *part, u_int parn, const partinfo_t *pold)
{
  u_int ret = 0;

  dbg_printf("powertec_validate_deletion(parn=%d)", parn);
  dbg_level_up();

  assert(part != NULL);
  assert(parn < part->nr_partitions);

  if (parn < 2)
    set_error("the first Filecore partition cannot be deleted");
  else
    ret = 1;

  dbg_level_down();
  dbg_printf("ret %svalid", ret ? "" : "in");

  return ret;
}

/* Prototype: u_int powertec_validate_partno(part_t *part, u_int parn)
 * Function : validate a partition number
 * Params   : part - partitionable device
 *          : parn - partition number
 * Returns  : FALSE if we reject the partition number
 */
static u_int
powertec_validate_partno(part_t *part, u_int parn)
{
  u_int ret = 0;

  assert(part != NULL);

  dbg_printf("powertec_validate_partno(parn=%d)", parn);
  dbg_level_up();

  if (parn < POWERTEC_NR_PARTITIONS)
    ret = 1;
  else
    set_error("the requested partition is invalid");

  dbg_level_down();
  dbg_printf("ret %svalid", ret ? "" : "in");

  return ret;
}

/* Prototype: ptyp_t powertec_nexttype(part_t *part, u_int parn, ptyp_t current, int dir)
 * Purpose  : return next valid partition type for an entry
 * Params   : part    - partitionable device
 *          : parn    - partition number
 *          : current - currently selected partition type
 *          : dir     - next/previous flag
 * Returns  : next valid partition type
 */
static ptyp_t
powertec_nexttype(part_t *part, u_int parn, ptyp_t current, int dir)
{
  ptyp_t ptype;
  s_int i;

  dbg_printf("powertec_nexttype(parn=%d, current=0x%X, %s)", parn, current,
	     dir > 0 ? "next" : dir < 0 ? "previous" : "???");
  dbg_level_up();
  assert(part != NULL);
  assert(parn < part->nr_partitions);

  for (i = 0; i < NR_TYPES - 1; i++)
    if (powertec_ptypes[i].type == current)
      break;

  if (powertec_ptypes[i].type == current) {
    i += dir;

    if (i < 0)
      i = 0;

    if (i > NR_TYPES - 1)
      i = NR_TYPES - 1;

    ptype = powertec_ptypes[i].type;
  } else
    ptype = ptyp_pt_empty;

  dbg_level_down();
  dbg_printf("ret 0x%X", ptype);

  return ptype;
}

scheme_t powertec_scheme = {
  "PowerTec",
  powertec_detect,
  powertec_readinfo,
  powertec_writeinfo,
  powertec_allocate,
  powertec_validate_change,
  powertec_validate_creation,
  powertec_validate_deletion,
  powertec_validate_partno,
  powertec_nexttype
};
