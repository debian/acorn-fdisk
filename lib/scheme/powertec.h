/*
 * lib/scheme/powertec.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_POWERTEC_H
#define SCHEME_POWERTEC_H

#define SCHEME_POWERTEC

typedef struct {
  u_int unused;
} powertec_data_t;

typedef struct {
  u_int idx;	/* offset into partition table			*/
  char  *type;	/* powertec type name (for unknown partitions)	*/
} powertec_part_t;

extern struct scheme powertec_scheme;

#endif
