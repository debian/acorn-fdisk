/*
 * lib/scheme/riscix.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef SCHEME_RISCiX_H
#define SCHEME_RISCiX_H

#define SCHEME_FILECORE_RISCiX

typedef struct {
  disc_record_t disc_record;	/* disc record			*/
  u8		boot_checksum;	/* checksum of boot sector	*/
} filecore_riscix_data_t;

typedef struct {
  u_int part_offset;		/* offset into partition table */
} filecore_riscix_part_t;

extern struct scheme filecore_riscix_scheme;

#endif
