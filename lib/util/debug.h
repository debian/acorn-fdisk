/*
 * lib/debug.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef LIB_DEBUG_H
#define LIB_DEBUG_H

/* Function: void dbg_level_up (void)
 * Purpose : increase the indentation level of debugs
 */
extern void dbg_level_up (void);

/* Function: void dbg_level_up (void)
 * Purpose : decrease the indentation level of debugs
 */
extern void dbg_level_down (void);

/* Function: void dbg_printf (const char *fmt, ...)
 * Purpose : write some debugging information
 * Params  : fmt - printf-like format string
 */
extern void dbg_printf (const char *fmt, ...);

/* Function: void dbg_memdump (void *data, int length)
 * Purpose : dump out a block of memory
 * Params  : data   - start of memory to dump
 *         : length - number of bytes to dump
 */
extern void dbg_memdump (void *data, int length);

#endif
