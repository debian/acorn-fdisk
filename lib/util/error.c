/*
 * lib/util/error.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <stdarg.h>
#include <stdio.h>

#include "util/error.h"

static char errmsg[2048];
static int error_set;

/* Function: void set_error (const char *fmt, ...)
 * Purpose : Set error string
 * Params  : fmt - printf-like format
 */
void set_error (const char *fmt, ...)
{
  va_list ap;

  if (fmt) {
    va_start (ap, fmt);
    vsprintf (errmsg, fmt, ap);
    va_end (ap);
    error_set = 1;
  } else {
    errmsg[0] = '\0';
    error_set = 0;
  }
}

/* Function: const char *get_error(void)
 * Purpose : Get error string
 * Returns : const pointer to error message buffer
 */
const char *get_error(void)
{
  error_set = 0;
  return errmsg;
}

/* Function: u_int is_error_set(void)
 * Purpose : check to see if an error is set
 * Returns : FALSE if not set
 */
u_int is_error_set(void)
{
  return error_set;
}

/* Function: void clear_error(void)
 * Purpose : clear error condition
 */
void clear_error(void)
{
  error_set = 0;
}
