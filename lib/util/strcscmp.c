#include <ctype.h>
#include <string.h>

int strcasecmp (const char *s1, const char *s2)
{
  int diff;

  do {
    diff = tolower(*s1) - tolower(*s2);

    if (diff)
      return diff;

    if (!*s1)
      return 0;

    s1 ++;
    s2 ++;
  } while (1);
}
