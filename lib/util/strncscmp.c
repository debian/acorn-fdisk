#include <ctype.h>
#include <string.h>

int strncasecmp (const char *s1, const char *s2, int len)
{
  int diff;

  diff = 0;

  do {
    if (--len < 0)
      return diff;

    diff = tolower(*s1) - tolower(*s2);

    if (diff)
      return diff;

    if (!*s1)
      return 0;

    s1 ++;
    s2 ++;
  } while (len);

  return 0;
}
