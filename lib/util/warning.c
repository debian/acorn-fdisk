/*
 * lib/util/warning.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>

#include "util/warning.h"

static char warning_message[2048];

/* Function: void issue_warning (const char *fmt, ...)
 * Purpose : Set warning string
 * Params  : fmt - printf-like format
 */
void issue_warning (const char *fmt, ...)
{
  va_list ap;

  if (fmt) {
    int characters = 0;
    char *p, *s, *e;

    va_start (ap, fmt);
    p = warning_message;
    p += sprintf(p, "Warning: ");
    vsprintf (p, fmt, ap);
    va_end (ap);

    for (p = s = e = warning_message;; p += 1) {
      if (!*p) {
      	if (characters)
      	  printf("%s\n", s);
        break;
      }
      characters += 1;
      if (isspace(*p))
        e = p;
      if (characters > 60) {
      	*e = '\0';
        printf("%s\n", s);
        s = (p = e) + 1;
        characters = 0;
      }
    }
  }
}
