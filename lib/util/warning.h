/*
 * lib/warning.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef WARNING_H
#define WARNING_H

#include "util/types.h"

/* Function: void issue_warning (const char *fmt, ...)
 * Purpose : Display a warning string
 * Params  : fmt - printf-like format
 */
void issue_warning (const char *fmt, ...);

#endif
